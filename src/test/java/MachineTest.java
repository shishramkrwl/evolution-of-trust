import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MachineTest {

    @Test
    public void shouldCheckForPlayer1CoOperatedPlayer2Cheat() {

        Machine machine = new Machine();

        int[] score = machine.calculate(Machine.COOPERATE, Machine.CHEAT);

        assertEquals(-1, score[0]);
        assertEquals(3, score[1]);
    }

    @Test
    public void shouldCheckForPlayer1CheatedPlayer2CoOperated() {

        Machine machine = new Machine();

        int[] score = machine.calculate(Machine.CHEAT, Machine.COOPERATE);

        assertEquals(3, score[0]);
        assertEquals(-1, score[1]);
    }

    @Test
    public void shouldCheckForBothPlayersCoOperated() {

        Machine machine = new Machine();

        int[] score = machine.calculate(Machine.COOPERATE, Machine.COOPERATE);

        assertEquals(2, score[0]);
        assertEquals(2, score[1]);
    }

    @Test
    public void shouldCheckForBothPlayersCheated() {

        Machine machine = new Machine();

        int[] score = machine.calculate(Machine.CHEAT, Machine.CHEAT);

        assertEquals(0, score[0]);
        assertEquals(0, score[1]);
    }
}
