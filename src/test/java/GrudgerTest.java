import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrudgerTest {

    @Test
    public void shouldReturnCooperateAsFirstMove() {
        Grudger grudger = new Grudger();
        assertEquals(Machine.COOPERATE, grudger.makeAMove());
    }

    @Test
    public void shouldReturnCooperateUntilOtherPlayerCheats() {
        Grudger grudger = new Grudger();
        assertEquals(Machine.COOPERATE, grudger.makeAMove());

        grudger.updateOpponentMove(Machine.CHEAT);
        assertEquals(Machine.CHEAT, grudger.makeAMove());

        grudger.updateOpponentMove(Machine.COOPERATE);
        assertEquals(Machine.CHEAT, grudger.makeAMove());
    }

}
