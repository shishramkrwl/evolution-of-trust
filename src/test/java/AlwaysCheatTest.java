import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlwaysCheatTest {

    @Test
    public void cheaterShouldAlwaysCheat() {
        Player cheater = new AlwaysCheat();
        assertEquals(Machine.CHEAT, cheater.makeAMove());
    }

    @Test
    public void cheaterShouldAlwaysCheatFor2Rounds() {
        Player cheater = new AlwaysCheat();
        assertEquals(Machine.CHEAT, cheater.makeAMove());
        assertEquals(Machine.CHEAT, cheater.makeAMove());
    }
}
