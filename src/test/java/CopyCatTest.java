import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CopyCatTest {

    @Test
    public void copyCatShouldReturnCooperateAsFirstMove() {
        CopyCat copyCat = new CopyCat();

        assertEquals(Machine.COOPERATE, copyCat.makeAMove());
    }

    @Test
    public void copyCatShouldCopyMovesFromSecondTimeOnwards() {
        CopyCat copyCat = new CopyCat();

        assertEquals(Machine.COOPERATE, copyCat.makeAMove());
        assertEquals(Machine.CHEAT, copyCat.makeAMove());
    }
}
