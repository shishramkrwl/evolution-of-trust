import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void makeAMoveForCoOperate() {

        ConsolePlayer player = new ConsolePlayer(new Scanner("1"));

        int move = player.makeAMove();

        assertEquals(Machine.COOPERATE, move);
    }

    @Test
    public void makeAMoveForCheat() {

        ConsolePlayer player = new ConsolePlayer(new Scanner("0"));

        int move = player.makeAMove();

        assertEquals(Machine.CHEAT, move);
    }


    @Test
    public void shouldStoreDefaultScore() {

        ConsolePlayer player = new ConsolePlayer(new Scanner("0"));

        assertEquals(0, player.getScore());
    }

    @Test
    public void shouldUpdateScoreForCoOperate() {

        ConsolePlayer player = new ConsolePlayer(new Scanner("0"));
        player.updateScore(2);

        assertEquals(2, player.getScore());
    }
}
