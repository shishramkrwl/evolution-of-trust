import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldReturnScoreAs2_2For1RoundForCooperateCooperate() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);

        Game game = new Game(player1, player2, machine);

        when(player1.makeAMove()).thenReturn(1);
        when(player2.makeAMove()).thenReturn(4);


        when(player1.getScore()).thenReturn(1);
        when(player2.getScore()).thenReturn(1);

        when(machine.calculate(1, 4)).thenReturn(new int[]{1, 2});

        game.play(1);

        verify(player1).makeAMove();
        verify(player2).makeAMove();

        verify(machine).calculate(1, 4);
        verify(player1).updateScore(1);
        verify(player2).updateScore(2);

    }

    @Test
    public void shouldRunForTwoRounds() {

        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Game game = new Game(player1, player2, machine);

        when(player1.makeAMove()).thenReturn(0).thenReturn(0);
        when(player2.makeAMove()).thenReturn(0).thenReturn(0);

        when(player1.getScore()).thenReturn(0);
        when(player2.getScore()).thenReturn(0);

        when(machine.calculate(0, 0)).thenReturn(new int[]{0, 0}).thenReturn(new int[]{0, 0});

        game.play(2);

        verify(player1, times(2)).makeAMove();
        verify(player2, times(2)).makeAMove();

        verify(machine, times(2)).calculate(0, 0);

        verify(player1, times(2)).updateScore(0);
        verify(player2, times(2)).updateScore(0);

    }
//
//
//    @Test
//    public void testAutomatedGamePlay() {
//        Game game = new Game(playerOne, playerTwo, machine);
//
//        ConsolePlayer cheater = mock(ConsolePlayer.class);
//        ConsolePlayer cooperator = mock(ConsolePlayer.class);
//
//        when(cheater.makeAMove()).thenReturn(Machine.CHEAT).thenReturn(Machine.CHEAT).thenReturn(Machine.CHEAT).thenReturn(Machine.CHEAT).thenReturn(Machine.CHEAT);
//        when(cooperator.makeAMove()).thenReturn(Machine.COOPERATE).thenReturn(Machine.COOPERATE).thenReturn(Machine.COOPERATE).thenReturn(Machine.COOPERATE).thenReturn(Machine.COOPERATE);
//
//        game.play(5, cheater, cooperator);
//
//        when(cheater.getScore()).thenReturn(15);
//        when(cooperator.getScore()).thenReturn(-5);
//
//        int[] score = game.result();
//        assertEquals(15, score[0]);
//        assertEquals(-5, score[1]);
//
//        verify(cheater, times(5)).makeAMove();
//        verify(cooperator, times(5)).makeAMove();
//
//        verify(cheater, times(6)).getScore();
//        verify(cooperator, times(6)).getScore();
//
//    }
}
