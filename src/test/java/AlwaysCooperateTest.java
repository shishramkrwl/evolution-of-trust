import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlwaysCooperateTest {

    @Test
    public void coorpoatorShouldAlwaysCoorporate() {
        Player cooporator = new AlwaysCooperate();
        assertEquals(Machine.COOPERATE, cooporator.makeAMove());
        assertEquals(Machine.COOPERATE, cooporator.makeAMove());
        assertEquals(Machine.COOPERATE, cooporator.makeAMove());
    }
}
