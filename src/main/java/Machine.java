public class Machine {

    public static int COOPERATE = 1;
    public static int CHEAT = 0;

    public int[] calculate(int playerOneInput, int playerTwoInput) {

        if (playerOneInput == COOPERATE && playerTwoInput == CHEAT) {
            return new int[]{-1, 3};
        } else if (playerOneInput == CHEAT && playerTwoInput == COOPERATE) {
            return new int[]{3, -1};
        } else if (playerOneInput == COOPERATE && playerTwoInput == COOPERATE) {
            return new int[]{2, 2};
        } else {
            return new int[]{0, 0};
        }
    }
}
