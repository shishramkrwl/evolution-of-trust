public interface Player {
    int CONSOLE = 0;
    int CHEATER = 1;
    int COOPERATOR = 2;

    int makeAMove();
    int getScore();
    void updateScore(int score);
}
