public interface OpponentObserver {
    public void updateOpponentMove(int opponentMove);
}
