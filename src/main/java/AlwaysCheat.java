public class AlwaysCheat implements Player {
    private int score;

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void updateScore(int score) {
        this.score = score;
    }

    @Override
    public int makeAMove() {
        return Machine.CHEAT;
    }
}
