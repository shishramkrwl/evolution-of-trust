public class CopyCat implements Player, OpponentObserver {
    private boolean isNewbie;
    private int opponentMove;

    public CopyCat() {
        this.isNewbie = true;
    }

    @Override
    public int makeAMove() {
        if (this.isNewbie) {
            this.isNewbie = false;
            return Machine.COOPERATE;
        }

        return opponentMove;
    }

    @Override
    public int getScore() {
        return 0;
    }

    @Override
    public void updateScore(int score) {

    }

    @Override
    public void updateOpponentMove(int opponentMove) {
        this.opponentMove = opponentMove;
    }
}
