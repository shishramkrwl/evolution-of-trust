import java.util.Scanner;

public class ConsolePlayer implements Player {
    private Scanner scanner;
    private int score;

    public ConsolePlayer(Scanner scanner) {
        this.scanner = scanner;
        this.score = 0;
    }

    public int makeAMove() {
        return scanner.nextInt();
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void updateScore(int score) {
        this.score = score;
    }
}
