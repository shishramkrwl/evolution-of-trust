public class Game {

    private Player playerOne;
    private Player playerTwo;
    private Machine machine;

    public Game(Player playerOne, Player playerTwo, Machine machine) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.machine = machine;
    }


    public static void main(String[] args) {
//        Game game = new Game(playerOne, playerTwo);
//        Scanner scanner = new Scanner(System.in);
//
//        int option = scanner.nextInt();
//
//        switch (option) {
//            case 0:
//                game.play(5, new AlwaysCheat(), new AlwaysCooperate());
//                System.out.println(game.result()[0] + ":" + game.result()[1]);
//                break;
//            case 1:
//                game.play(2, new AlwaysCheat(), new CopyCat());
//                System.out.println(game.result()[0] + ":" + game.result()[1]);
//                break;
//            default:
//                game.play(scanner.nextInt(), new ConsolePlayer(scanner), new ConsolePlayer(scanner));
//                System.out.println(game.result()[0] + ":" + game.result()[1]);
//        }
    }

    public void play(int round) {

        while (round > 0) {
            int playerOneLastMove = playerOne.makeAMove();
            int playerTwoLastMove = playerTwo.makeAMove();

            int[] score = machine.calculate(
                    playerOneLastMove,
                    playerTwoLastMove
            );

            // player1 is OpponentObserver, player 1 .updateOpponentMove(score[1])
            // player2 is copycat, player2.updateOpponentMove(score[0])

            this.playerOne.updateScore( score[0]);
            this.playerTwo.updateScore( score[1]);

            round--;
        }
    }

    public int[] result() {
        return new int[]{this.playerOne.getScore(), this.playerTwo.getScore()};
    }

}
