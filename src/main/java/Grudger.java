public class Grudger implements Player, OpponentObserver{
    private int score;
    private boolean isHoldingGrude;

    public Grudger() {
        this.isHoldingGrude = false;
    }

    public int makeAMove() {
        return isHoldingGrude ? Machine.CHEAT : Machine.COOPERATE;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void updateScore(int score) {
        this.score += score;
    }

    @Override
    public void updateOpponentMove(int opponentMove) {
        if (opponentMove == Machine.CHEAT) {
            this.isHoldingGrude = true;
        }
    }
}
